#include <stdio.h>

const int g_first_array[] = {1,2,3};
const int g_second_array[] = {4,5,6};

long scalar_product(const int first_array[], const int second_array[], size_t size){
	long result = 0;
	size_t i;
	for (i = 0; i < size; i++){
		result+=first_array[i]*second_array[i];
	}
	return result;
}

int main(){
	printf(
            "The scalar product is: %ld\n",
            scalar_product(g_first_array, g_second_array, sizeof(g_first_array) / sizeof(int))
         );
	return 0;
}
