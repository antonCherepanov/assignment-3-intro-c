#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

bool is_prime(const unsigned long number){
	if(number <= 1UL){
		return false;
	}else if(number % 2UL == 0){
		if(number == 2UL){
			 return true;
		}else{
			return false;
		}
	}else{
		bool result = true;
		const int sqrt_number = (int)floor(sqrt((double) number));
		int i;
		for (i=3; i<=sqrt_number;i+=2){
			if(number % i == 0) result = false;
		}
		return result;
	}
}

bool is_unsigned_long(const char input[]){
	int i=0;
	for (i = 0; input[i] != '\0'; i++) {
		if(!isdigit(input[i])) {
			return false;
		}
	}
	return true;
}

int main(){
	unsigned long argument;
	char input[20];
	puts("Enter a number to check");
	while(true){
		scanf("%s", input);
		if(strcmp(input, "quit") == 0 || strcmp(input, "q") == 0) return 0;
		if(is_unsigned_long(input)){
			argument = strtoul(input, NULL, 10);
			printf("%lu is %sprime\n", argument, is_prime(argument) ? "" : "not ");
		}else{
			puts("You've provided an improper argument");
		}
	}
}
